// Global Variables
const int ledPin = 7;
const int sensorPin = 0;
const int red = 3;
const int yellow = 4;
const int blue = 5;

void setup()
{
  pinMode(ledPin, OUTPUT);
  for (int i = red; i <= blue; i++)
  {
    pinMode(i, OUTPUT);
  }
  Serial.begin(9600);
}

void loop()
{
  // standardPotentiometer();
  // temperatureSensor();
  thermometer();
  // lightSensor();
}

// Works for the standard potentiometer (Blue) and for the soft pot
void standardPotentiometer ()
{
  int ledDelay = analogRead(sensorPin);
  Serial.println(ledDelay);

  digitalWrite(ledPin, HIGH);
  delay(ledDelay);
  digitalWrite(ledPin, LOW);
  delay(ledDelay);
}

// Works for the temperature sensor only
void temperatureSensor ()
{
  // Get voltage from temp sensor
  float voltage = analogRead(sensorPin) * 0.004882814;
  // Convert voltage to Celsius
  float tempC = (voltage - 0.5) * 100.0;
  // Convert to Fahrenheit
  float tempF = (tempC * 9.0 / 5.0) + 32.0;

  // Print Results
  Serial.print(voltage);
  Serial.print(" volts, ");
  Serial.print(tempC);
  Serial.print(" degrees C, ");
  Serial.print(tempF);
  Serial.print(" degrees F");
  Serial.println();

  delay(1000);
}

// Powers on LEDs based on the current temperature
void thermometer ()
{
  float tempF = ((((analogRead(sensorPin) * 0.004882814)
                   - .5) * 100.0) * 9.0 / 5.0) + 32.0;

  if (tempF >= 85.0)
  {
    digitalWrite(red, HIGH);
    digitalWrite(blue, LOW);
    digitalWrite(yellow, LOW);
  }
  else if (tempF >= 75.0)
  {
    digitalWrite(red, LOW);
    digitalWrite(blue, LOW);
    digitalWrite(yellow, HIGH);
  }
  else
  {
    digitalWrite(red, LOW);
    digitalWrite(blue, HIGH);
    digitalWrite(yellow, LOW);
  }

  Serial.println(tempF);

  delay(100);
}

// Works for the light sensor and the flex sensor
void lightSensor ()
{
  // Get value
  int reading = analogRead(sensorPin);
  // Display it
  Serial.println(reading);
  // Pause
  delay(500);
}
